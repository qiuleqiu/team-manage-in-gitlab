module.exports = exports = {
    //一般是官网域名，部分企业内网开发这里要改成内网配置的域名，后面不加斜杠（/）
    siteUrl: 'https://gitlab.com',
    //每页返回的提交数量（请求步长），默认为最大值，一次请求最多100条，这是gitlab的限制，已经实现滚动请求。 可改成比100小的数字实现小步快跑，但要大于0
    step: 100,
    //过滤提交，提交title中包含以下任意内容都会被忽略，不会抓取
    filter: ['Merge remote-tracking branch', 'Merge branch'],
    //在网页版登录后获取请求头的cookie复制粘贴到这里，针对登录用户有访问权限的非公开项目
    cookie: '_gitlab_session=6d4fe939a7473799d191c2ffd65317a1',
    keepDelete: false,//todo:获取数据是否保留移除的代码
};