const fs = require('fs');
const path = require('path');
const runGitlab = require('./gitlab.js');

async function getAllData(authors) {
    let personObj = {}, dataArr;
    for (let i in authors) {
        personObj[i] = {};
        if (authors.hasOwnProperty(i)) {
            for (let j = 0; j < authors[i].projects.length; j++) {
                personObj[i][authors[i].projects[j].projectName] = {};
                for (let k = 0; k < authors[i].projects[j].branchs.length; k++) {
                    dataArr = await runGitlab(authors[i].projects[j].projectName, i, authors[i].projects[j].branchs[k], authors[i].time);
                    personObj[i][authors[i].projects[j].projectName][authors[i].projects[j].branchs[k]] = dataArr;
                }
            }
        }
    }
    return personObj;
}

async function allDataToHtml(authors, fileName) {
    const beginTime = new Date().getTime();
    data = await getAllData(authors);
    let nameLi = '', nameContent = '', isFirstPerson;
    for (let personName in data) {
        if (!data.hasOwnProperty(personName)) {
            continue;
        }
        nameLi += `<li class="${isFirstPerson === undefined ? 'current' : ''}">${personName}</li>`;
        nameContent += `<div class="item" style="${isFirstPerson === undefined ? 'display: block;' : ''}">`;
        if (isFirstPerson === undefined) {
            isFirstPerson = false;
        }
        let commit = {}, changeline = 0, commitNum = 1;
        for (let projectName in data[personName]) {
            if (!data[personName].hasOwnProperty(projectName)) {
                continue;
            }
            nameContent += `<h2 class="click-for-show" title="点击折叠展开">项目 ${projectName} 的所有提交变动</h2><div for="project">`;
            const prjObj = data[personName][projectName];
            for (let branchName in prjObj) {
                if (!prjObj.hasOwnProperty(branchName)) {
                    continue;
                }
                let branchContent = '', branchChangeLine = 0;
                for (let i = 0; i < prjObj[branchName].length; i++) {
                    const thisBranchObj = prjObj[branchName][i];
                    commit[thisBranchObj.title] = true;
                    changeline += thisBranchObj.changeline;
                    branchChangeLine += thisBranchObj.changeline;
                    const branchData = thisBranchObj.diffData;
                    branchContent += `<h4 class="click-for-show" title="点击折叠展开">${commitNum++}.${thisBranchObj.title}（${thisBranchObj.changeline}）</h4><div class="commit-content" style="height:0;">`;
                    for (let j = 0; j < branchData.length; j++) {
                        const firstRowTs = branchData[j].new.match(/(\t|\s)+/);
                        branchContent += `<pre title="${branchData[j].file} ${thisBranchObj.time}"><div class="new"><code contenteditable="false" class="language-javascript" >${solveChar(branchData[j].new, firstRowTs)}</code></div>`;
                        if (branchData[j].old) {
                            branchContent += `<div class="old"><code contenteditable="false" class="language-javascript" >${solveChar(branchData[j].old, firstRowTs)}</code></div>`;
                        }
                        branchContent += `</pre>`
                    }
                    branchContent += `</div>`
                }
                nameContent += `<h3 class="click-for-show" title="点击折叠展开">分支 ${branchName} 的所有提交变动(${branchChangeLine})</h3><div for="branch">`;
                nameContent += branchContent;
                nameContent += `</div>`
            }
            nameContent += `</div>`
        }
        //提交次数过滤了不同分支合并后产生的相同提交
        nameContent += `<div class="count">汇总: 提交次数（<strong>${Object.keys(commit).length}</strong>）; 改变行数（<strong>${changeline}</strong>）</div>`
        nameContent += `</div>`;
    }
    const html = fs.readFileSync('./template/template.html', 'utf8');
    const sp = html.replace('{{person}}', nameLi).split('{{personContent}}');
    const directory = './output';
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory);
    }
    const _fileName = directory + '/result_' + (fileName ? fileName.replace(/[\s:：]+/g, '-') : new Date().getTime());
    fs.writeFileSync(_fileName + '.html', sp[0] + nameContent + sp[1]);
    //fs.writeFileSync(_fileName + '.html', html.replace('{{person}}', nameLi).replace('{{personContent}}', nameContent));//此代码在某些特殊情况返回的html有问题造成错位，原因未知
    fs.writeFileSync(_fileName + '.json', JSON.stringify(data, undefined, 4));
    console.log('写入文件成功, 总耗时：', ((new Date().getTime() - beginTime) / 1000 / 60).toFixed(2), '分钟');
    console.log('已生成可视化页面，请复制到浏览器访问：', 'file:///' + path.join(__dirname, _fileName + '.html').replace(/\\/g, '/'));
    return data;
}

function solveChar(string, ts) {
    if(ts) {
        ts = ts[0];
        const regTransform = ts.replace(/\\/g, '\\');
        string = string.replace(new RegExp('^' + regTransform), '').replace(new RegExp('(?<=\\n)' + regTransform, 'g'), '');
    }
    return string.replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/\t/g, '&nbsp;');
}

//allDataToHtml(JSON.parse(fs.readFileSync('./output/result_2022-08.json', 'utf8')));//for test
module.exports = exports = {allDataToHtml, getAllData};