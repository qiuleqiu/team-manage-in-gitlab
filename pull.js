const {allDataToHtml, getAllData} = require('./solve.js');
/**
 * 根据用户名来获取提交信息，可多个用户名，每个用户多个项目，每个项目多个分支来获取汇总
 **/
const timeRange = '2022 08';
const authors = {
    "qiuleqiu": {
        time: timeRange,
        projects: [
            {
                projectName: 'test',
                branchs: ['main']
            }
        ],
    },
    "张三": {
        time: timeRange,
        projects: [
            {
                projectName: 'test',
                branchs: ['main']
            }
        ],
    },
    "李四": {
        time: timeRange,
        projects: [
            {
                projectName: 'test',
                branchs: ['main']
            }
        ],
    },
};
allDataToHtml(authors, timeRange);