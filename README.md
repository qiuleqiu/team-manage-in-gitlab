# Team Manage on Gitlab

通过时间节点获取用户在gitlab上的提交数据，包括提交次数，代码改变行数，变动前后的代码，返回一个json对象，基于json对象可进行任意数据分析，实现自动化代码审查。
(Get the user's submission data on gitlab through the time node, including the number of submission times, the number of lines of code change, the code before and after the change, return a JSON object and output the visual interface. Based on the JSON object, any data analysis can be carried out to realize automatic code review)

## Getting started

```
npm i
```
```
node pull
```
## Setting

### setting.js
```
{
	//一般是官网域名，部分企业内网开发这里要改成内网配置的域名，后面不加斜杠（/）
	siteUrl: 'https://gitlab.com',
	//每页返回的提交数量（请求步长），默认为最大值，一次请求最多100条，这是gitlab的限制，已经实现滚动请求。 可改成比100小的数字实现小步快跑，但要大于0
	step: 100,
	//过滤提交，提交title中包含以下任意内容都会被忽略，不会抓取
	filter: ['Merge remote-tracking branch', 'Merge branch'],
	//如果是获取私有项目，在网页版登录后获取请求头的cookie复制粘贴到这里
	cookie: '',
}
```
### pull.js

```
const timeRange = '2022 08';
const names = {
	"qiuleqiu": {
		time: timeRange,
		projects: [
			{
				projectName: 'test',
				branchs: ['main']
			}
		],
	}
};
```
注意时间范围(timeRange)的格式：

格式如下（按照年月日时分秒依次序排序，数字不足两位的需要在前面补0，不同单位时间数字必须用空格或者：分隔开）：
```
    2022 09 10 09:14:25 - 2022 10 20 09:14:25    注:2022年9月10日9点14分25秒到2022年10月20日9点14分25秒
    2022 09 10 09:14 - 2022 10 20 09:14          注:2022年9月10日9点14分到2022年10月20日9点14分
    2022 09 10 09 - 2022 10 20 09                注:2022年9月10日9点到2022年10月20日9点
    2022 09 10 - 2022 10 20                      注:2022年9月10日到2022年10月20日
    2022 09 - 2022 10                            注:2022年9月到2022年10月
    2022 - 2023                                  注:2022年到2023年（2022一整年）
    2022 09 12                                   注:2022年9月12日到2022年9月13日（一整天）
    2022 09                                      注:2022年9月到2022年10月（9月一整月）
    2022                                         注:2022一整年
    截止日期只是个时间尾数，抓取内容不包含
```
注意：

1.单次获取范围请控制在输出内容不超过1000条，否则会影响前端可视化页面性能，建议每次最大按月范围获取。

2.solvePage函数里的爬取url，不同的类型项目可能有些许修改，如果获取不到内容要注意这个链接