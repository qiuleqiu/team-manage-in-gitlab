var tab_list = document.querySelector(".tab_list");
var lis = tab_list.querySelectorAll("li");
var items = document.querySelectorAll('.tab_con .item');

//绑定事件
for (var i = 0; i < lis.length; i++) {
    //开始给li设置索引号
    lis[i].setAttribute('index', i);
    lis[i].onclick = function () {
        for (var i = 0; i < lis.length; i++) {
            lis[i].className = '';
        }
        this.className = 'current';
        var index = this.getAttribute('index');//获取索引号，找到对应的选项卡
        for (var i = 0; i < items.length; i++) {
            items[i].style.display = 'none';
        }
        items[index].style.display = 'block';
    }
}

var h = document.querySelectorAll(".click-for-show");
for (var i = 0; i < h.length; i++) {
    h[i].onclick = function () {
        this.style.color = '#3857af';
        if (this.nextElementSibling.style.height !== '0px') {
            this.nextElementSibling.style.height = '0px';
        } else {
            this.nextElementSibling.style.height = 'max-content';
        }
    }
}

// 当容器滚动 20px 出现"返回顶部" 按钮
for(let i = 0; i < items.length; i++) {
    (function(i) {
        items[i].onscroll = function () {
            scrollFunction(items[i]);
        };
    })(i);
}

function scrollFunction(_parent) {
    let btn = document.getElementById("myBtn");
    if (_parent.scrollTop > 20) {
        btn.style.display = "block";
    } else {
        btn.style.display = "none";
    }
}

// 点击按钮，返回顶部
function topFunction() {
    let _parent = Array.prototype.filter.call(items, function(item) {
        return item.style.display !== 'none';
    })[0];
    _parent.scrollTop = 0;
}