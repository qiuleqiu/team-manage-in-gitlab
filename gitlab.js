const {request} = require('urllib');
const cheerio = require('cheerio');
const setting = require('./setting.js');

module.exports = exports = getProjectCommitData;

/**
 * project gitlab上面的项目名称
 * author 代码提交人的名字
 * branch 分支
 * dateRange 提交的时间范围，格式如下（按照年月日时分秒依次序排序，数字不足两位的需要在前面补0，不同单位时间数字必须用空格或者：分隔开）：
 2022 09 10 09:14:25 - 2022 10 20 09:14:25    注:2022年9月10日9点14分25秒到2022年10月20日9点14分25秒
 2022 09 10 09:14 - 2022 10 20 09:14          注:2022年9月10日9点14分到2022年10月20日9点14分
 2022 09 10 09 - 2022 10 20 09                注:2022年9月10日9点到2022年10月20日9点
 2022 09 10 - 2022 10 20                      注:2022年9月10日到2022年10月20日
 2022 09 - 2022 10                            注:2022年9月到2022年10月
 2022 - 2023                                  注:2022年到2023年（2022一整年）
 2022 09 12                                   注:2022年9月12日到2022年9月13日（一整天）
 2022 09                                      注:2022年9月到2022年10月（9月一整月）
 2022                                         注:2022一整年
 * 不传此参数则不限制时间；截止日期只是个时间尾数，抓取内容不包含
 **/
async function getProjectCommitData(project, author, branch, dateRange, callback) {
    if (!project || !author || !branch) {
        return console.error('请传完整的参数！');
    }
    if (typeof dateRange !== 'string') {
        throw new Error('时间范围参数错误！，请参照2022 09 10 09:14:25-2022 10 20 09:14:25格式！');
    }
    let _data = [], offset = 0,
        step = setting.step;

    async function solvePage(offset) {
        //注意这个地址公开项目和私有项目前半部分可能不一样;
        //const url = setting.siteUrl + '/' + project + '/-/commits/' + branch + '?author=' + author + '&limit='+step+'&offset=' + offset;//公开项目
        const url = setting.siteUrl + '/' + author + '/' + project + '/-/commits/' + branch + '?author=' + author + '&limit=' + step + '&offset=' + offset;//私有项目
        const html = await getPageContent(url);
        const {data, finish} = await solveData(html, dateRange, setting.filter);
        _data = _data.concat(data);
        console.log('是否完成所有请求：', finish);
        if (!finish) {
            offset += step;
            return solvePage(offset);
        }
    }

    await solvePage(offset);
    callback && callback(_data);
    return _data;
}

let getPageCount = 0;

async function getPageContent(url, options) {
    try {
        if (!options) {
            options = {};
        }
        let userAgent = 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 qiuleqiu/robot Firefox/1.5.0.12';
        let reqData = Object.assign({
            dataType: 'text',
            agent: userAgent,
            timeout: 100000//防止某些页面奇葩慢，100s够了吧~
        }, options);
        reqData.headers = Object.assign({
            'User-Agent': userAgent,
            cookie: setting.cookie,
            referer: setting.siteUrl
        }, options.headers || {})
        const {data, res} = await request(url, reqData);
        getPageCount++;
        console.log(getPageCount + '.获取页面数据成功：' + url);
        return data;
    } catch (e) {
        const tip = '获取页面' + url + '没有返回数据, 请检查你的网络和cookie设置！';
        console.error(tip);
        return '<div>' + tip + '</div>'
    }

}

function parseTime(range) {
    if (typeof range === 'number') {
        return range;
    }
    let basic = new Array(6).fill('00');
    const sp = range.match(/\b\d+\b/g);
    basic.forEach(function (item, index) {
        basic[index] = sp[index] || '00';
    });
    return +(basic.join(''));
}

async function solveData(html, dateRange, filterTitle) {
    const $ = cheerio.load(html);
    const commits = $('.commit-content');
    let data = [], finish = false;
    if (!filterTitle) {
        filterTitle = [];
    }
    if (typeof html !== 'string') {
        console.error('页面数据非字符串无法分析！');
        finish = true;
        return {data, finish};
    }
    filterTitle.push('Merge remote-tracking branch', 'Merge branch');//去掉合并的提交
    let $_this, changeCount;
    if (commits.length) {
        for (let i = 0; i < commits.length; i++) {
            $_this = commits.eq(i);
            const time = $_this.find('.js-timeago');
            const datetime = $(time).attr('datetime').replace(/[TZ]/g, ' ');
            if (dateRange) {
                const sp = dateRange.split('-');
                if (!sp[1]) {
                    sp[1] = sp[0].replace(/(\d+)$/, function (a, b) {
                        const newVal = (+a + 1) + '';
                        return newVal.length === 1 ? '0' + newVal : newVal;
                    });
                }
                if (parseTime(datetime) < parseTime(sp[0])) {
                    //gitlab是倒序排序，所以只要访问到了比起始时间小的条目即可提前结束遍历
                    finish = true;
                    break;
                }
                if (parseTime(datetime) >= parseTime(sp[1])) {
                    continue;
                }
            }
            const text = $_this.find('a.commit-row-message').text();
            let matchedFilter;
            filterTitle.forEach(function (title) {
                if (text.indexOf(title) > -1) {
                    matchedFilter = true;
                }
            });
            if (matchedFilter) {
                continue;
            }
            changeCount = 0;
            const href = $_this.find('.js-onboarding-commit-item').attr('href');
            const diffHtml = await getPageContent(setting.siteUrl + href + '/diff_files');
            const diffData = solveDiff(diffHtml);
            //看看是否有分页
            const $pagination = $_this.find('.g1-pagination');
            if ($pagination.length) {
                const $li = $pagination.find('li.js-pagination-page');
                for (let i = 0; i < $li.length; i++) {
                    const href = $li.eq(i).find('a').attr('href');
                    if (href !== '#') {
                        const _diffHtml = await getPageContent(setting.siteUrl + href);
                        diffData.push(solveDiff(_diffHtml))
                    }
                }
            }


            data.push({
                title: text,
                url: setting.siteUrl + href,
                time: datetime,
                diffData,
                changeline: changeCount,
            })
        }
    } else {
        finish = true;
    }

    function solveDiff(diffPageHtml) {
        const diff = [];
        if (typeof diffPageHtml !== 'string') {
            console.error('页面数据非字符串无法分析！');
            return diff;
        }
        const $ = cheerio.load(diffPageHtml);
        let hadSolved = {};
        $('.diff-content td.line_content.new').each(function () {
            const lineId = $(this).parent().attr('id');
            const lineNum = lineId.split('_');
            let newContent = $(this).text(),
                oldContent = '';
            if (hadSolved[lineId]) {
                return;
            }
            changeCount++;
            hadSolved[lineId] = true;
            $(this).closest('table').find('td.line_content.new, td.line_content.old').each(function () {
                const _lineId = $(this).parent().attr('id');
                const _lineNum = _lineId.split('_');
                if (lineId !== _lineId) {
                    if (lineNum[1] === _lineNum[1]) {
                        newContent += $(this).text();
                        changeCount++;
                        hadSolved[_lineId] = true;
                    } else if (lineNum[2] === _lineNum[2]) {
                        oldContent += $(this).text();
                        hadSolved[_lineId] = true;
                    }
                }
            });
            diff.push({
                new: solve_n(newContent),
                old: solve_n(oldContent),
                file: $(this).closest('.diff-file').find('.file-title-name').text().replace(/\n+/g, '')
            })
        });
        return diff;
    }

    function solve_n(string) {
        return string.replace(/\n{2,}/g, '\n\n').replace(/^\n+/, '').replace(/\n+$/, '');
    }

    return {data, finish};
}